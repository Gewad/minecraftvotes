# MinecraftVotes

Aims to let Minecraft servers implement a voting system.
Votes will have certain parameters for the server administrator to set:
  - Who can vote (playergroups)
  - When votes are collected (Start datetime & end datetime)
  - How votes are collected (chat, interactive inventory or in game buttons)